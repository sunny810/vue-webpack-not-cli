var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: "./src/main.js",
  output: {
    publicPath: '/dist/',
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js'
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/
      },
      {
        test: /\.vue$/,
        loader: 'vue'
      }
    ]
  },
  devServer : {
      colors: true, //终端中输出结果为彩色
      historyApiFallback: true, //不跳转
      inline: true ,
      hot : true,
      progress: true,
      port : 6060//实时刷新
  },
  resolve: {
    alias: {
      vue: 'vue/dist/vue.js'
    },
    extensions: ['', '.js', '.vue']
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin()
  ]
}
